//pages
const introPage = document.getElementById('introPage');
const symptomsPage = document.getElementById('symptomsPage');
const travelPage = document.getElementById('travelPage');
const currentPage = document.getElementById('currentPage');
const agePage = document.getElementById('agePage');
const puiPage = document.getElementById('puiPage');
const pumPage = document.getElementById('pumPage');
const knowMore = document.getElementById('knowMore');
const sendPage = document.getElementById('sendPage');
const formAdmin = document.getElementById('formAdmin');
const relayDoh = document.getElementById('relayDoh');
const puiMore = document.getElementById('puiMore');
const pumMore = document.getElementById('pumMore');
const successInfo = document.getElementById('successInfo');
const nonePage = document.getElementById('nonePage');
const loader = document.getElementById('loader');
const api = document.getElementById('api');


//buttons
const startBtn = document.getElementById('startBtn');
const knowMoreBtn = document.getElementById('knowMoreBtn');
const symptomsBtn = document.getElementById('symptomsBtn');
const ageBtn = document.getElementById('ageBtn');
const currentBtn = document.getElementById('currentBtn');
const puiBtn = document.getElementById('puiBtn');
const puiSendBtn = document.getElementById('puiSendBtn');
const pumBtn = document.getElementById('pumBtn');
const pumSendBtn = document.getElementById('pumSendBtn');
const sendBtn = document.getElementById('sendBtn');
const loginBtn = document.getElementById('loginBtn');
const logoutBtn = document.getElementById('logoutBtn');
const formBtn = document.getElementById('formBtn');
const backToBtn = document.getElementById('backToBtn');
const backPumBtn = document.getElementById('backPumBtn');
const backPuiBtn = document.getElementById('backPuiBtn');
const successBtn = document.getElementById('successBtn');
const backHome = document.getElementById('backHome');
const bbHome = document.getElementById('bbHome');
const clearAll = document.getElementById('clearAll');
const backNone = document.getElementById('backNone');
const toApi = document.getElementById('toApi');


//checkboxes
const fever = document.getElementById('fever');
const cough = document.getElementById('cough');
const colds = document.getElementById('colds');
const difficulty = document.getElementById('difficulty');
const none1 = document.getElementById('none1');
const outside = document.getElementById('outside');
const contact = document.getElementById('contact');
const none2 = document.getElementById('none2');
const above = document.getElementById('above');
const below = document.getElementById('below');
const yes = document.getElementById('yes');
const no = document.getElementById('no');

var patient = [];


 function open() {
 		var retrievedPatient = JSON.parse(localStorage.getItem('patient'));

		retrievedPatient.forEach(function(eachPatient){

		var newRow =  document.createElement('tr');

		newRow.innerHTML = `<td>${eachPatient.name}</td>
							<td>${eachPatient.address}</td>
							<td>${eachPatient.email}</td>
							<td>${eachPatient.contacts}</td>
							<td>${eachPatient.select}</td>`

 		document.getElementById('patientDetails').appendChild(newRow);
 });
};

window.onload = open;

knowMoreBtn.addEventListener('click', function(){
	introPage.classList.remove('d-flex');
	introPage.classList.add('d-none');
	knowMore.classList.remove('d-none');
	knowMore.classList.add('d-block');
	formAdmin.classList.remove('d-flex');
	formAdmin.classList.add('d-none');
	symptomsPage.classList.remove('d-flex');
	symptomsPage.classList.remove('d-none');
	symptomsPage.classList.remove('d-flex');
	symptomsPage.classList.remove('d-none');
	travelPage.classList.remove('d-flex');
	travelPage.classList.remove('d-none');
	currentPage.classList.remove('d-flex');
	currentPage.classList.add('d-none');
	agePage.classList.remove('d-flex');
	agePage.classList.add('d-none');
	puiPage.classList.remove('d-flex');
	puiPage.classList.add('d-none');
	pumPage.classList.remove('d-flex');
	pumPage.classList.add('d-none');
	sendPage.classList.remove('d-flex'); 
	sendPage.classList.add('d-none'); 
	relayDoh.classList.remove('d-flex'); 
	relayDoh.classList.add('d-none'); 
	puiMore.classList.remove('d-flex'); 
	puiMore.classList.add('d-none'); 
	pumMore.classList.remove('d-flex'); 
	pumMore.classList.add('d-none'); 
	successInfo.classList.remove('d-flex'); 
	successInfo.classList.add('d-none'); 
	api.classList.remove('d-flex'); 
	api.classList.add('d-none'); 
});


formBtn.addEventListener('click', function(){
	introPage.classList.remove('d-flex');
	introPage.classList.add('d-none');
	formAdmin.classList.remove('d-none');
	formAdmin.classList.add('d-flex');
	symptomsPage.classList.remove('d-flex');
	symptomsPage.classList.remove('d-none');
	symptomsPage.classList.remove('d-flex');
	symptomsPage.classList.remove('d-none');
	travelPage.classList.remove('d-flex');
	travelPage.classList.remove('d-none');
	currentPage.classList.remove('d-flex');
	currentPage.classList.add('d-none');
	agePage.classList.remove('d-flex');
	agePage.classList.add('d-none');
	puiPage.classList.remove('d-flex');
	puiPage.classList.add('d-none');
	pumPage.classList.remove('d-flex');
	pumPage.classList.add('d-none');
	knowMore.classList.remove('d-block');
	knowMore.classList.add('d-none');
	sendPage.classList.remove('d-flex'); 
	sendPage.classList.add('d-none'); 
	relayDoh.classList.remove('d-flex'); 
	relayDoh.classList.add('d-none'); 
	puiMore.classList.remove('d-flex'); 
	puiMore.classList.add('d-none'); 
	pumMore.classList.remove('d-flex'); 
	pumMore.classList.add('d-none'); 
	successInfo.classList.remove('d-flex'); 
	successInfo.classList.add('d-none'); 
	api.classList.remove('d-flex'); 
	api.classList.add('d-none'); 
});


toApi.addEventListener('click', function(){
	introPage.classList.remove('d-flex');
	introPage.classList.add('d-none');
	formAdmin.classList.remove('d-flex');
	formAdmin.classList.add('d-none');
	symptomsPage.classList.remove('d-flex');
	symptomsPage.classList.remove('d-none');
	symptomsPage.classList.remove('d-flex');
	symptomsPage.classList.remove('d-none');
	travelPage.classList.remove('d-flex');
	travelPage.classList.remove('d-none');
	currentPage.classList.remove('d-flex');
	currentPage.classList.add('d-none');
	agePage.classList.remove('d-flex');
	agePage.classList.add('d-none');
	puiPage.classList.remove('d-flex');
	puiPage.classList.add('d-none');
	pumPage.classList.remove('d-flex');
	pumPage.classList.add('d-none');
	knowMore.classList.remove('d-block');
	knowMore.classList.add('d-none');
	sendPage.classList.remove('d-flex'); 
	sendPage.classList.add('d-none'); 
	relayDoh.classList.remove('d-flex'); 
	relayDoh.classList.add('d-none'); 
	puiMore.classList.remove('d-flex'); 
	puiMore.classList.add('d-none'); 
	pumMore.classList.remove('d-flex'); 
	pumMore.classList.add('d-none'); 
	successInfo.classList.remove('d-flex'); 
	successInfo.classList.add('d-none'); 
	api.classList.remove('d-none'); 
	api.classList.add('d-flex'); 
});



function showLoader(){
		introPage.classList.remove('d-flex');
		introPage.classList.add('d-none');
		loader.classList.remove('d-none');
		loader.classList.add('d-flex');
};

function firstDiv(){
	setTimeout("showLoader()", 500)
};

function hideLoader(){
	loader.classList.remove('d-flex');
	loader.classList.add('d-none');
	symptomsPage.classList.remove('d-none');
	symptomsPage.classList.add('d-flex');
};

function secondDiv(){
	setTimeout("hideLoader()", 3000)
};

function anotherShowLoader(){
	sendPage.classList.remove('d-flex');
	sendPage.classList.add('d-none');
	loader.classList.remove('d-none');
	loader.classList.add('d-flex');
};

function thirdDiv(){
	setTimeout("anotherShowLoader()", 200)
};

function anotherHideLoader(){
	loader.classList.remove('d-flex');
	loader.classList.add('d-none');
	successInfo.classList.remove('d-none');
	successInfo.classList.add('d-flex');
};

function fourthDiv(){
	setTimeout("anotherHideLoader()", 3000)
};

function uncheck(){
 var uncheck=document.getElementsByTagName('input');
 for(var i=0;i<uncheck.length;i++)
 {
  if(uncheck[i].type=='checkbox')
  {
   uncheck[i].checked=false;
  }
 }
};


backBtn.addEventListener('click', function(){
	window.location.reload();
});

backToBtn.addEventListener('click', function(){
	window.location.reload();
});

backHome.addEventListener('click', function(){
	window.location.reload();
});

bbHome.addEventListener('click', function(){
	window.location.reload();
});


clearAll.addEventListener('click', function(){
	localStorage.clear();
	document.getElementById('patientDetails').innerHTML = " ";
});

backNone.addEventListener('click', function(){
	window.location.reload();
});


symptomsBtn.addEventListener('click', function(){
	if(!fever.checked && !cough.checked && !colds.checked && !difficulty.checked && !none1.checked){
		alert("Please Check at least one!");
	}else {
	symptomsPage.classList.remove('d-flex');
	symptomsPage.classList.add('d-none');
	travelPage.classList.remove('d-none');
	travelPage.classList.add('d-flex');
	}
});


travelBtn.addEventListener('click', function(){
	if(!outside.checked && !contact.checked && !none2.checked){
		alert("Please Check at least one!");
	}else {
	travelPage.classList.remove('d-flex');
	travelPage.classList.add('d-none');
	agePage.classList.remove('d-none');
	agePage.classList.add('d-flex');
	}
});

ageBtn.addEventListener('click', function(){
	if(!above.checked && !below.checked){
		alert("Please Check at least one!");
	}else if (above.checked && below.checked){
		alert("You can only check one!");
	} else {
	agePage.classList.remove('d-flex');
	agePage.classList.add('d-none');
	currentPage.classList.remove('d-none');
	currentPage.classList.add('d-flex');
	}
});

function pumPop(){
		currentPage.classList.remove('d-flex');
		currentPage.classList.add('d-none');
	  	pumPage.classList.remove('d-none');
		pumPage.classList.add('d-flex');
};

function nonePop(){
		currentPage.classList.remove('d-flex');
		currentPage.classList.add('d-none');
	  	nonePage.classList.remove('d-none');
		nonePage.classList.add('d-flex');
};

currentBtn.addEventListener('click', function(){
	if(!yes.checked && !no.checked){
		alert("Please Check at least one!");
	}else if (yes.checked && no.checked){
		alert("You can only check one!");
  	}else if(none1.checked && outside.checked && above.checked && no.checked){
		pumPop();
	}else if(none1.checked && outside.checked && below.checked && no.checked){
		pumPop();
    }else if(none1.checked && contact.checked && above.checked && no.checked){
    	pumPop();
	}else if(none1.checked && contact.checked && below.checked && no.checked){
		pumPop();
	}else if(none1.checked && outside.checked && contact.checked && above.checked && no.checked){
		pumPop();
	}else if(none1.checked && outside.checked && contact.checked && below.checked && no.checked){
		pumPop();
	}else if(none1.checked && none2.checked && above.checked && no.checked){
		nonePop();
	}else if(none1.checked && none2.checked && below.checked && no.checked){
		nonePop();
	}else {
		currentPage.classList.remove('d-flex');
		currentPage.classList.add('d-none');
	  	puiPage.classList.remove('d-none');
		puiPage.classList.add('d-flex');
	}

});


puiBtn.addEventListener('click', function() {
	puiPage.classList.remove('d-flex');
	puiPage.classList.add('d-none');
	pumPage.classList.remove('d-flex');
	pumPage.classList.add('d-none');
	puiMore.classList.remove('d-none');
	puiMore.classList.add('d-flex');

})

puiSendBtn.addEventListener('click', function() {
	puiPage.classList.remove('d-flex');
	puiPage.classList.add('d-none');
	pumPage.classList.remove('d-flex');
	pumPage.classList.add('d-none');
	sendPage.classList.remove('d-none');
	sendPage.classList.add('d-flex');

})

pumBtn.addEventListener('click', function(){
	pumPage.classList.remove('d-flex');
	pumPage.classList.add('d-none');
	pumMore.classList.remove('d-none');
	pumMore.classList.add('d-flex');
});

pumSendBtn.addEventListener('click', function(){
	pumPage.classList.remove('d-flex');
	pumPage.classList.add('d-none');
	sendPage.classList.remove('d-none');
	sendPage.classList.add('d-flex');
});


backPumBtn.addEventListener('click', function(){
	window.location.reload();
});

backPuiBtn.addEventListener('click', function(){
	window.location.reload();
});

backToBtn.addEventListener('click', function(){
	window.location.reload();
});


successBtn.addEventListener('click', function(){
		successInfo.classList.remove('d-flex');
		successInfo.classList.add('d-none');
		introPage.classList.remove('d-none');
		introPage.classList.add('d-flex');
});




sendBtn.addEventListener('click', function() {

	const name = document.getElementById('name').value;
	const address = document.getElementById('address').value;
	const email = document.getElementById('email').value;
	const contacts = document.getElementById('contacts').value;
	const select = document.getElementById('select').value;



	if(name.length === 0 || address.length === 0 || email.length === 0 || contact.length === 0){
			alert("All fields must be filled.")
		} else {
			document.getElementById('patientDetails').innerHTML = "";

			patient = JSON.parse(localStorage.getItem('patient')) || [];

			var patientList = {
				name: name,
				address: address,
				email: email,
				contacts: contacts,
				select: select
			}

			patient.push(patientList);


			localStorage.setItem('patient', JSON.stringify(patient))

			patient.forEach(function(eachPatient, dataIndex){
			const newRow =  document.createElement('tr');

			newRow.innerHTML = `<td>${eachPatient.name}</td>
								<td>${eachPatient.address}</td>
								<td>${eachPatient.email}</td>
								<td>${eachPatient.contacts}</td>
								<td>${eachPatient.select}</td>`

			localStorage.setItem('patientDetails',JSON.stringify(newRow))

			document.getElementById('patientDetails').appendChild(newRow);
		});
	};
});
	



loginBtn.addEventListener('click', function() {
	const uName = "teamjutsu";
	const pWord = "teamjutsu2020";

	const userName = document.getElementById('userName').value;
	const passWord = document.getElementById('passWord').value;

	if(userName !== uName || passWord !== pWord){
		alert("Wrong Username or Password");
	} else {
		formAdmin.classList.remove('d-flex');
		formAdmin.classList.add('d-none');
		relayDoh.classList.remove('d-none');
		relayDoh.classList.add('d-flex');

	}
});


const api_url = 'https://api.covid19api.com/summary';
async function getInfo() {
	const response = await fetch(api_url);
	const data = await response.json();
	const newConfirmed = data.Global.NewConfirmed;
	const newDeaths = data.Global.NewDeaths;
	const newRecovered = data.Global.NewRecovered;
	const totalConfirmed = data.Global.TotalConfirmed;
	const totalDeaths = data.Global.TotalDeaths;
	const totalRecovered = data.Global.TotalRecovered;


	document.getElementById('totalConfirmed').textContent = totalConfirmed;
	document.getElementById('totalDeaths').textContent = totalDeaths;
	document.getElementById('totalRecovered').textContent = totalRecovered;
	document.getElementById('newConfirmed').textContent = newConfirmed;
	document.getElementById('newDeaths').textContent = newDeaths;
	document.getElementById('newRecovered').textContent = newRecovered;
}

getInfo()



